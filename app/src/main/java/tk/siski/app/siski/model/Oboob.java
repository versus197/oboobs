package tk.siski.app.siski.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;
import tk.siski.app.siski.Const;
import tk.siski.app.siski.SiskiApp;
import tk.siski.app.siski.util.TinyDB;

public class Oboob implements OBoobInteractor {

    public String current_stream_url, current_fav_url;
    Stack history_urls, future_urls;
    ArrayList<String> fav_urls;
    TinyDB tinydb;
    private int favPosition = 0;
    private static Oboob _instance = null;

    private Oboob() {
        tinydb = new TinyDB(SiskiApp.context());
        history_urls = new Stack();
        future_urls = new Stack();
        fav_urls = null;
        resumeCurrentStreamUrl();
        fav_urls = tinydb.getList("favorite");
    }

    public static synchronized Oboob getInstance() {
        if (_instance == null)
            _instance = new Oboob();
        return _instance;
    }

    private void pushHistory(String url){
        history_urls.push(new String(url));
    }

    private String popHistory(){
        try{
            return (String) history_urls.pop();}
        catch (EmptyStackException e) {
            return null;
        }
    }

    private String popFuture(){
        try{
        return (String) future_urls.pop();}
        catch (EmptyStackException e) {
            return null;
        }
    }

    @Override
    public void pushFuture(String url){
        if(url != null) {
            future_urls.push(new String(url));
        }
    }

    @Override
    public int getSizeFuture() {
        return future_urls.size();
    }

    @Override
    public boolean containsHistory(String url) {
        return history_urls.contains(url);
    }

    @Override
    public String getCurrentStreamUrl(){
        return current_stream_url;
    }

    @Override
    public void nextStream(){
        if(current_stream_url != null) pushHistory(current_stream_url);
        current_stream_url = popFuture();
    }

    @Override
    public void backStream(){
        String prev_url = current_stream_url;
        current_stream_url = popHistory();
        if(current_stream_url == null){
            current_stream_url = prev_url;
        }
        else {
            pushFuture(prev_url);
        }
    }

    @Override
    public String getCurrentFavUrl(){
        return current_fav_url;
    }

    @Override
    public void addCurrentUrlToFav(){
        if(!fav_urls.contains(current_stream_url)){
            fav_urls.add(current_stream_url);
            Log.d("Oboobs", "Fav add "+ current_stream_url);
            saveFav();
        }
        else{
            Log.d("Oboobs","Fav Dublicate detected");
        }
    }

    @Override
    public void deleteCurrentFav() {
        fav_urls.remove(current_fav_url);
        saveFav();
    }

    @Override
    public void  deleteCurrentFavFromStream() {
        fav_urls.remove(current_stream_url);
        Log.d("Oboobs", "Fav detected " + current_stream_url);
        saveFav();
    }

    @Override
    public void nextFav(){
        if(favPosition < getSizeFavPics()){
        current_fav_url = fav_urls.get(favPosition);
        favPosition++;
        checkFavIndex();
        }
    }

    @Override
    public void backFav(){
        if(favPosition > 0){
            try {
                current_fav_url = fav_urls.get(favPosition);
                favPosition--;
            } catch (IndexOutOfBoundsException e) {
                checkFavIndex();
            }
        }
        checkFavIndex();
    }

    @Override
    public int getSizeFavPics(){
        return fav_urls.size();
    }

    @Override
    public void saveCurrentStreamUrl() {
        tinydb.putString("lastUrl", current_stream_url);
    }

    @Override
    public void resumeCurrentStreamUrl() {
        current_stream_url = tinydb.getString("lastUrl");
        if(current_stream_url.isEmpty()) current_stream_url = Const.DEFAULT_URL;
    }

    @Override
    public boolean isCurrentUrlFav() {
        return fav_urls.contains(current_stream_url);

    }

    private void saveFav(){
        tinydb.putList("favorite", fav_urls);
    }

    public void checkFavIndex(){
        if(favPosition < 0) {
            favPosition = 0;
        }
        if(favPosition > (getSizeFavPics()-1) ){
            favPosition = getSizeFavPics()-1;
        }
        current_fav_url = fav_urls.get(favPosition);
    }

    @Override
    public void initFavPosition() {
        this.favPosition = getSizeFavPics()-1;
        this.current_fav_url = fav_urls.get(favPosition);
    }
}
