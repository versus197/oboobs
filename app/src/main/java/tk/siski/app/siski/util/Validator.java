package tk.siski.app.siski.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validator {

    public static boolean isPhone(String phone){
        Pattern p = Pattern.compile("^[+]?[0-9]{10,13}$");
        Matcher m = p.matcher(phone);
        if (m.matches()){
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean checkNetworkStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = connectivityManager.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }
}
