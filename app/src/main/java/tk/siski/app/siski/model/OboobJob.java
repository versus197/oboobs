package tk.siski.app.siski.model;

import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tk.siski.app.siski.Const;

public class OboobJob extends Job {

    private static final int Priority_LOW = 0;

    public OboobJob() {
        super(new Params(Priority_LOW).requireNetwork().groupBy("getBoobs"));
    }

    @Override
    public void onAdded() {}

    @Override
    public void onRun() throws Throwable {
        Oboob oboobs = Oboob.getInstance();
        OkHttpClient client = new OkHttpClient();
        Response response = client
                .newCall(new Request.Builder()
                            .url(Const.api_url+Const.api_get_random)
                            .build())
                .execute();
        try {
            JSONArray urls = new JSONArray(response.body().string());
            for (int i = 0; i < urls.length(); i++) {
                JSONObject jsonobj = urls.getJSONObject(i);
                Log.d("GET JSON OBJECT", jsonobj.getString("preview"));
                if(!oboobs.containsHistory(jsonobj.getString("preview"))) {
                    oboobs.pushFuture(Const.media_url + jsonobj.getString("preview"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCancel() {}

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }
}
