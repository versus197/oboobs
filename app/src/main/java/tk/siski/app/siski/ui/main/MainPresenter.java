package tk.siski.app.siski.ui.main;

import tk.siski.app.siski.Const;

public interface MainPresenter {

    void onCreate();

    void onPause();

    void onResume();

    void addCurrentToFav();

    void swipe(Const.Swipe swipe);

    void back();
    void forward();
    void renew();
    void toFavorite();
    void toStream();

    void onUnFavorite();

    public void InitWith(MainActivity activity);
}
