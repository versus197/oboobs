package tk.siski.app.siski.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import tk.siski.app.siski.util.Validator;

public class OBoobsWidget extends AppWidgetProvider {

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d("WIDGET", "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        if(Validator.checkNetworkStatus(context)) {
            Log.d("WIDGET", "onUpdate");
            ComponentName thisWidget = new ComponentName(context,
                    OBoobsWidget.class);
            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
            Intent intent = new Intent(context.getApplicationContext(),
                    OBoobsWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
            context.startService(intent);
        }
    }
}

