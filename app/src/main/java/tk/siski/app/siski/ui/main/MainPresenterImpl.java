package tk.siski.app.siski.ui.main;

import android.util.Log;
import com.path.android.jobqueue.JobManager;
import tk.siski.app.siski.Const;
import tk.siski.app.siski.R;
import tk.siski.app.siski.SiskiApp;
import tk.siski.app.siski.model.OBoobInteractor;
import tk.siski.app.siski.model.Oboob;
import tk.siski.app.siski.model.OboobJob;

import static tk.siski.app.siski.Const.State.*;

public class MainPresenterImpl implements MainPresenter{

    MainView rootView;
    private Const.State state;
    private OBoobInteractor oboobs;
    MainActivity activity;
    JobManager jobManager;

    public MainPresenterImpl(MainView rootView) {
        jobManager = SiskiApp.getInstance().getJobManager();
        this.rootView = rootView;
        oboobs = Oboob.getInstance();
        setState(Stream);
        downloadStream();
        rootView.hideProgressDialog();
    }

    @Override
    public void onCreate() {
        downloadStream();
        Log.d("Presenter", "oboobs.getSizeFavPics() = " + oboobs.getSizeFavPics());
    }

    @Override
    public void onPause() {
        oboobs.saveCurrentStreamUrl();
    }

    @Override
    public void onResume() {
        oboobs.resumeCurrentStreamUrl();
        rootView.showPic(getCurrentUrl(), isFav());
    }

    @Override
    public void InitWith(MainActivity activity) {
        activity = activity;
    }

    public MainActivity getActivity() {
        return activity;
    }

    public void setState(Const.State value) {
        switch (value) {
            case Stream:
                state = Stream;
                break;
            case Favorite:
                if(oboobs.getSizeFavPics() > 0){
                    state = Favorite;
                    oboobs.initFavPosition();
                } else {
                    setState(Stream);
                }
                break;
        }
        Log.d("State", "State = " + state.toString());

    }

    @Override
    public void addCurrentToFav() {
        if(state == Stream) {
            oboobs.addCurrentUrlToFav();
        }
    }

    @Override
    public void swipe(Const.Swipe swipe) {
        switch (swipe) {
            case Up:
                if(isStateStream()){
                    setState(Favorite);
                    rootView.setMainTitle(R.string.fav);
                }
                else{
                    oboobs.deleteCurrentFav();
                    if(oboobs.getSizeFavPics() == 0){
                        setState(Stream);
                    }
                    else{
                        oboobs.checkFavIndex();
                        oboobs.nextFav();
                        rootView.showDeletePic();
                    }
                }
                break;
            case Down:
                if(isStateStream()){
                    downloadStream();
                    oboobs.nextStream();
                } else {
                    setState(Stream);
                    rootView.setMainTitle(R.string.steam);
                }
                break;
            case Left:
                if(isStateStream()){
                    oboobs.nextStream();
                    Log.d("SWIPE", "SizeFuture = " + String.valueOf(oboobs.getSizeFuture()));
                    if( oboobs.getSizeFuture() == 1){
                        downloadStream();
                    }
                }
                else{
                    oboobs.nextFav();
                }

                break;
            case Right:
                if(isStateStream()){
                    oboobs.backStream();
                }
                else{
                    oboobs.backFav();
                }
                break;
        }
        rootView.showPic(getCurrentUrl(), isFav());

    }

    @Override
    public void back() {
        swipe(Const.Swipe.Right);
    }

    @Override
    public void forward() {
        swipe(Const.Swipe.Left);

    }

    @Override
    public void renew() {
        swipe(Const.Swipe.Down);
    }

    @Override
    public void toFavorite() {
        if(oboobs.getSizeFavPics() > 0) {
            Log.d("Presenter", "oboobs.getSizeFavPics() = " + oboobs.getSizeFavPics());
            setState(Favorite);
            rootView.showPic(getCurrentUrl(), isFav());
            rootView.setMainTitle(R.string.fav);
        }else {
            toStream();
        }
    }

    @Override
    public void toStream() {
        setState(Stream);
        rootView.setMainTitle(R.string.steam);
        rootView.showPic(getCurrentUrl(), isFav());

    }

    @Override
    public void onUnFavorite() {
        if(isStateStream()) {
            oboobs.deleteCurrentFavFromStream();
        }else {
            oboobs.deleteCurrentFav();
        }
    }

    private String getCurrentUrl() {
        String currentUrl = null;
        switch (state) {
            case Stream:
                currentUrl = oboobs.getCurrentStreamUrl();
                break;
            case Favorite:
                currentUrl = oboobs.getCurrentFavUrl();
                break;
        }
        return currentUrl;
    }

    public boolean isFav() {
        if(state == Favorite){
            return true;
        }
        else {
            return oboobs.isCurrentUrlFav();
        }
    }

    private void downloadStream()  {
        if(oboobs.getSizeFuture() < 80){
            jobManager.addJobInBackground(new OboobJob());
        }
    }

    public boolean isStateStream() {
        if(state == Stream) {
            return true;}
        else {
            return false;}
    }



}
