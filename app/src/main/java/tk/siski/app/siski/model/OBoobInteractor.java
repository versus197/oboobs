package tk.siski.app.siski.model;


public interface OBoobInteractor {

    // STREAM
    public String getCurrentStreamUrl();
    public void saveCurrentStreamUrl();
    public void resumeCurrentStreamUrl();
    public void nextStream();
    public void backStream();
    public void pushFuture(String url);
    public int getSizeFuture();
    public boolean containsHistory(String url);

    // FAVORITE
    public String getCurrentFavUrl();
    public int getSizeFavPics();
    public void nextFav();
    public void backFav();
    public void addCurrentUrlToFav();
    public void deleteCurrentFav();
    public void deleteCurrentFavFromStream();
    public boolean isCurrentUrlFav();
    public void checkFavIndex();
    public void initFavPosition();


}
