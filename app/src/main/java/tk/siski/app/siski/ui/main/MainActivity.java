package tk.siski.app.siski.ui.main;

import android.os.Bundle;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;
import com.androidquery.AQuery;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import tk.siski.app.siski.Const;
import tk.siski.app.siski.R;
import tk.siski.app.siski.util.Validator;

import static tk.siski.app.siski.SiskiApp.context;

public class MainActivity extends AppCompatActivity implements MainView{

    Animation animationFadeIn, animationFadeOut;
    AQuery aq;
    GestureDetector gestureDetector;
    MainPresenter presenter;
    ProgressDialog progressDialog;
    @InjectView(R.id.firstImageView)
    ImageView firstImageView;
    //@InjectView(R.id.adView)
    AdView mAdView;

    @InjectView(R.id.toolbar_bottom)
    Toolbar mToolbar;

    Handler h;
    boolean slideshow =false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_start_new);
        ButterKnife.inject(this);
        showProgressDialog();
        h = new Handler();
        if(!Validator.checkNetworkStatus(context())){
            Toast.makeText(this,"Нет интернет соединения!", Toast.LENGTH_SHORT).show();
            finish();
        }
        setTitle(R.string.steam);
        presenter = new MainPresenterImpl(this);
        presenter.InitWith(this);
        presenter.onCreate();
        aq = new AQuery(this);
        animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        animationFadeOut = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        firstImageView.setBackgroundColor(Color.TRANSPARENT);
        firstImageView.animate().setDuration(2000);
        aq.id(R.id.firstImageView).image(R.drawable.sila);
        //aq.id(R.id.firstImageView).clicked(this, "stopSlideShow");
        mToolbar.inflateMenu(R.menu.menu_start);
        showToFav(true);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener(){
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.d("TOOLBAR", "ITEM"+ item.getItemId());
                switch (item.getItemId()) {
                    case R.id.action_back:
                        mToolbar.getMenu().findItem(R.id.action_fav).setVisible(true);
                        mToolbar.getMenu().findItem(R.id.action_unfav).setVisible(false);
                        presenter.back();
                        break;
                    case R.id.action_forward:
                        mToolbar.getMenu().findItem(R.id.action_fav).setVisible(true);
                        mToolbar.getMenu().findItem(R.id.action_unfav).setVisible(false);
                        presenter.forward();
                        break;
                    case R.id.action_slide_show:
                        if(slideshow){
                            stopSlideShow();
                        }
                        else {
                            startSlideShow();
                        }
                        break;
                    case R.id.action_renew:
                        stopSlideShow();
                        presenter.renew();
                        showToFav(true);
                        break;
                    case R.id.action_unfav:
                        presenter.onUnFavorite();
                        mToolbar.getMenu().findItem(R.id.action_fav).setVisible(true);
                        mToolbar.getMenu().findItem(R.id.action_unfav).setVisible(false);
                        break;
                    case R.id.action_fav:
                        mToolbar.getMenu().findItem(R.id.action_fav).setVisible(false);
                        mToolbar.getMenu().findItem(R.id.action_unfav).setVisible(true);
                        onFavorite();
                        break;
                    case R.id.action_tofav:
                        stopSlideShow();
                        presenter.toFavorite();
                        showToFav(false);
                        break;
                    case R.id.action_toflow:
                        stopSlideShow();
                        presenter.toStream();
                        showToFav(true);
                        break;
                }
                return true;
            }
        });


        gestureDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return false;}

            @Override
            public void onLongPress(MotionEvent e) {}

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                float sensitvity = 50;
                if ((e1.getX() - e2.getX()) > sensitvity) {
                    Log.d("SISKI", "Swipe Left");
                    presenter.swipe(Const.Swipe.Left);
                } else if ((e2.getX() - e1.getX()) > sensitvity) {
                    Log.d("SISKI", "Swipe Right");
                    presenter.swipe(Const.Swipe.Right);
                } else if ((e1.getY() - e2.getY()) > sensitvity) {
                    Log.d("SISKI", "Swipe UP");
                    presenter.swipe(Const.Swipe.Up);
                } else if ((e2.getY() - e1.getY()) > sensitvity) {
                    Log.d("SISKI", "Swipe DOWN");
                    presenter.swipe(Const.Swipe.Down);
                }
                return false;
            }
        });
    }



    public void showToFav(boolean isStream){
        if(isStream){
            mToolbar.getMenu().findItem(R.id.action_tofav).setVisible(true);
            mToolbar.getMenu().findItem(R.id.action_toflow).setVisible(false);
        }
        else{
            mToolbar.getMenu().findItem(R.id.action_tofav).setVisible(false);
            mToolbar.getMenu().findItem(R.id.action_toflow).setVisible(true);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
        firstImageView.clearAnimation();
        if (mAdView != null) {
            mAdView.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    public void onFavorite(){
        presenter.addCurrentToFav();
        Toast.makeText(this, "Эти сиськи мне нравятся", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPic(String url, boolean isFav) {
        aq.id(R.id.firstImageView).animate(animationFadeOut).image(url, true, false).animate(animationFadeIn);
        if(!isFav) {
            mToolbar.getMenu().findItem(R.id.action_fav).setVisible(true);
            mToolbar.getMenu().findItem(R.id.action_unfav).setVisible(false);
        }
        else{
            mToolbar.getMenu().findItem(R.id.action_fav).setVisible(false);
            mToolbar.getMenu().findItem(R.id.action_unfav).setVisible(true);
        }
    }

    @Override
    public void showAds() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("9704756F083F799240CCA051CED8AEE0")
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void showDeletePic() {
        Toast.makeText(this,"Cиськи удалены из любимых", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setMainTitle(int RTitle) {
        setTitle(RTitle);
    }


    public void showProgressDialog() {
        progressDialog = ProgressDialog.show(this, "Загрузка новых сисек", "Спокойствие только спокойствие", true);
    }

    public void hideProgressDialog() {
        Log.d("Activity", "progressDialog dismiss");
        progressDialog.dismiss();
    }

    Runnable slideShow = new Runnable() {
        public void run() {
            Log.d("MainActivity", "slideShow");
            presenter.forward();
            // планирует сам себя через 100 мсек
            h.postDelayed(slideShow, 5000);
        }
    };

    public void stopSlideShow() {
        if(slideshow) {
            setTitle(R.string.steam);
            h.removeCallbacks(slideShow);
            slideshow = false;
        }
    }

    private void startSlideShow() {
        if(!slideshow) {
            setTitle(R.string.slideshow);
            h.post(slideShow);
            slideshow = true;
        }
    }

}