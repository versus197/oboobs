package tk.siski.app.siski.ui.main;


public interface MainView {

    public void showPic(String url, boolean isFav);

    public void showAds();

    public void showDeletePic();

    public void setMainTitle(int RTitle);

    public void hideProgressDialog();


}
