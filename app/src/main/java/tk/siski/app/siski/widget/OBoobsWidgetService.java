package tk.siski.app.siski.widget;

import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.widget.RemoteViews;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import tk.siski.app.siski.Const;
import tk.siski.app.siski.R;
import tk.siski.app.siski.SiskiApp;
import tk.siski.app.siski.model.Oboob;
import tk.siski.app.siski.ui.main.MainActivity;
import tk.siski.app.siski.util.Validator;


public class OBoobsWidgetService extends IntentService {

    public OBoobsWidgetService() {
        super("siski");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(Validator.checkNetworkStatus(SiskiApp.context())){
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this
                    .getApplicationContext());

            int[] allWidgetIds = intent
                    .getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
            for (int widgetId : allWidgetIds) {
                RemoteViews remoteViews = new RemoteViews(this
                        .getApplicationContext().getPackageName(),
                        R.layout.widget);

                final Oboob ob = Oboob.getInstance();
                InputStream is = null;
                String url = null;
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client
                        .newCall(new Request.Builder()
                                .url(Const.api_url+Const.api_get_random)
                                .build())
                        .execute();
                    JSONArray urls = new JSONArray(response.body().string());
                        JSONObject jsonobj = urls.getJSONObject(0);
                        url = (Const.media_url + jsonobj.getString("preview"));
                    response = client
                            .newCall(new Request.Builder()
                                    .url(url)
                                    .build())
                            .execute();
                    is = response.body().byteStream();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                remoteViews.setImageViewBitmap(R.id.widgetImageView, BitmapFactory.decodeStream(is));
                if(url!=null){
                    ob.current_stream_url = url;
                    ob.saveCurrentStreamUrl();
                }
                Intent mainIntent = new Intent(this.getApplicationContext(), MainActivity.class);
                mainIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
                mainIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
                PendingIntent pIntent = PendingIntent.getActivity(this.getApplicationContext(), widgetId,
                        mainIntent, 0);
                remoteViews.setOnClickPendingIntent(R.id.widgetImageView, pIntent);
                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }
    }


}
